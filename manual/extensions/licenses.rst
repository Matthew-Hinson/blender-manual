.. index:: Licenses

.. Mark as "orphan" until extensions is out of beta.

:orphan:

##################
Extension Licenses
##################

The `Blender Extensions Platform <https://extensions.blender.org>`__ only supports
free and open source extensions compatible with Blender's license:
`GNU General Public License v3.0 or later <https://spdx.org/licenses/GPL-2.0-or-later.html>`__.

This allows extensions to be packed with Blender and distributed in compliance
with the governing principles of the `Blender license <https://www.blender.org/about/license/>`__.


GNU General Public License v2.0
===============================

Themes have no strong license requirements, so it is suggested to use `GNU General Public License v2.0 or later <https://spdx.org/licenses/GPL-2.0-or-later.html>`__.

For add-ons the recommended license is also `GNU General Public License v2.0 or later <https://spdx.org/licenses/GPL-2.0-or-later.html>`__.
However third-party libraries may require a different compatible license.

More GPL and LGPL Licenses
++++++++++++++++++++++++++

In those cases a few variant versions of GNU GPL are also accepted. Here are all the GPL and LGPL accepted licenses:

- `GNU General Public License v2.0 or later <https://spdx.org/licenses/GPL-2.0-or-later.html>`__
- `GNU General Public License v3.0 or later <https://spdx.org/licenses/GPL-3.0-or-later.html>`__.
- `GNU Lesser General Public License v2.1 or later <https://spdx.org/licenses/LGPL-2.1-or-later.html>`__.
- `GNU Lesser General Public License v3.0 or later <https://spdx.org/licenses/LGPL-3.0-or-later.html>`__.

Creative Commons
================

The most popular creative commons license for assets is either `Public Domain (CC0) <https://spdx.org/licenses/CC0-1.0.html>`__ 
or `CC-BY <https://spdx.org/licenses/CC-BY-SA-4.0.html>`__.

Other supported creative commons licenses are:

- `CC0-1.0 Universal (CC0 1.0) Public Domain Dedication <https://spdx.org/licenses/CC0-1.0.html>`__.
- `Creative Commons Attribution 1.0 Generic <https://spdx.org/licenses/CC-BY-1.0.html>`__.
- `Creative Commons Attribution 2.0 Generic <https://spdx.org/licenses/CC-BY-2.0.html>`__.
- `Creative Commons Attribution 2.5 Generic <https://spdx.org/licenses/CC-BY-2.5.html>`__.
- `Creative Commons Attribution 3.0 Unported <https://spdx.org/licenses/CC-BY-3.0.html>`__.
- `Creative Commons Attribution 4.0 International <https://spdx.org/licenses/CC-BY-4.0.html>`__.
- `Creative Commons Attribution Share Alike 1.0 Generic <https://spdx.org/licenses/CC-BY-SA-1.0.html>`__.
- `Creative Commons Attribution Share Alike 2.0 Generic <https://spdx.org/licenses/CC-BY-SA-2.0.html>`__.
- `Creative Commons Attribution Share Alike 2.5 Generic <https://spdx.org/licenses/CC-BY-SA-2.5.html>`__.
- `Creative Commons Attribution Share Alike 3.0 Unported <https://spdx.org/licenses/CC-BY-SA-3.0.html>`__.
- `Creative Commons Attribution Share Alike 4.0 International <https://spdx.org/licenses/CC-BY-SA-4.0.html>`__.

More Compatible Licenses
========================

In some exceptional cases other licenses may be required. Extensions are still accepted under these licenses:

- `BSD 1-Clause "Simplified" License <https://spdx.org/licenses/BSD-1-Clause.html>`__.
- `BSD 2-Clause "Simplified" License <https://spdx.org/licenses/BSD-2-Clause.html>`__.
- `BSD 3-Clause "New" or "Revised" License <https://spdx.org/licenses/BSD-3-Clause.html>`__.
- `Boost Software License 1.0 <https://spdx.org/licenses/BSL-1.0.html>`__.
- `MIT License <https://spdx.org/licenses/MIT.html>`__.
- `MIT No Attribution <https://spdx.org/licenses/MIT-0.html>`__.
- `Mozilla Public License 2.0 <https://spdx.org/licenses/MPL-2.0.html>`__.
- `Pixar License <https://spdx.org/licenses/Pixar.html>`__.
- `Zlib License <https://spdx.org/licenses/Zlib.html>`__.