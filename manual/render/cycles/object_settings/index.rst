
###################
  Object Settings
###################

Settings for objects and object data.

.. toctree::
   :maxdepth: 2

   object_data.rst
   light_linking.rst
   adaptive_subdiv.rst
   cameras.rst
