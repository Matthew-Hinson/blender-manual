
********
Box Mask
********

.. reference::

   :Mode:      Sculpt Mode
   :Tool:      :menuselection:`Toolbar --> Box Mask`

Creates a new :doc:`Mask </sculpt_paint/sculpting/editing/mask>`
based on a :ref:`box selection <tool-select-box>`.
Hold :kbd:`Ctrl` to subtract from the mask instead.

This tool is also accessible as a :ref:`shortcut operator <bpy.ops.paint.mask_box_gesture>` on :kbd:`B`.


Tool Settings
=============

Front Faces Only
   Only creates a mask on the faces that face towards the view.
